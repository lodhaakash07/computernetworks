# Computer Networks

## About Repository
* This repository contains c++ simulation of computer networks concepts learn in class.
* Object Oriented Concepts are not used
* Importance is given to readability rather than optimization
* Codes are well commented for understanding
* All Assumptions are mentioned in the codes

## Topics Included
* Byte Stuffing
* Bit Stuffing

All projects tested in CodeBlocks Ide in ubuntu14.04

